
# Запуск:

* создайте пустой .env.local
* `docker-compose up --build --force-recreate -d`
* `docker exec -u www-data -it test-symfony composer install`
* `docker exec -u www-data -it test-symfony bin/console doctrine:migrations:migrate`
* http://localhost:8181/login
